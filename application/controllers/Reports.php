<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function index()
	{
		
		
		$data['title']		=	'View and compare Reports';
		$data['heading']	=	'View and compare Reports';
		$data['icon']		=	'fa fa-search';
		$this->load->view('front/includes/header',$data);
		$this->load->view('front/includes/sidebar',$data);
		$this->load->view('front/reports',$data);
		$this->load->view('front/includes/footer',$data);
	}
	
	
}

