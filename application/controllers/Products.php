<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function index()
	{
		
		
		$data['title']		=	'Products';
		$data['heading']	=	'Products';
		$data['icon']		=	'fa fa-cube';
		
		$data['products']	=	$this->lib->get_table('products',array('product_id'=>'desc'),NULL,100);
		
		$sort					=	$this->input->get('sort');
		if($sort!=''){
			$sorts			=	explode('_',$sort);
			$data['products']	=	$this->lib->get_table('products',array($sorts[0]=>$sorts[1]));
		}
		
		
		$this->load->view('front/includes/header',$data);
		$this->load->view('front/includes/sidebar',$data);
		$this->load->view('front/products',$data);
		$this->load->view('front/includes/footer',$data);
	}
	
	
	
	
	
	
	public function export($sku){
		$this->load->model('products_model');
		
		$this->db->select(array('product_date','product_name','product_barcode','product_sku','product_stock'));
		$this->db->order_by('product_id','desc');
		$query	=	$this->db->get('products');
		if($query->num_rows()==0){
			$report	=	$this->products_model->generate_csv($query);
			if($report){
			redirect(base_url($report));
			exit();
			}
		}else{
		$this->lib->redirect_msg('No record to export','warning','products');
		}
		
	}
	
	public function test(){
		echo date('d-M-Y',strtotime('1-Nov-2012'));
	}
	
	
	
	function readExcel()
	{
        $this->load->library('csvreadernew');
        $result =   $this->csvreadernew->parse_file('static/inventrycsv.csv');

        $data['csvData'] =  $result;
		foreach($data['csvData'] as $rdata){
			if($rdata['SKU_Product_Id']==''){
			continue;	
			}
			echo "<pre>";
			print_r($rdata);
			echo "</pre>";
		}
	}
	
	
	
	public function import(){
		$this->load->view('front/ajax/import_product');
	}
	
	public function save(){
		$data	=	$this->input->post();
		
		$path	=	'static/front/csv/';
		if (!is_dir($path)) {
			mkdir($path);
		}
		
			if($_FILES['csvProduct']!=''){
			
			
			$upload_file	=	$this->lib->upload_file($path,'csvProduct');
			if($upload_file){
				$this->load->library('csvreader');
				$result =   $this->csvreader->parse_file($upload_file);
				
				foreach($result as $rdata){
					
					$rdata['product_ts']					= strtotime($rdata['product_date']);
					$rdata['product_added_on']		= time();
					
					
					$this->db->insert('products',$rdata);
					
				}
				
				$this->lib->redirect_msg('Product imported and saved successfully','success','products');
			}else{
				$this->lib->redirect_msg('Error in uploading product','Warning','products');
			}
			
			
			}else{
				$this->lib->redirect_msg('Error in uploading csv file','danger','products');
			}
		
	}
}

