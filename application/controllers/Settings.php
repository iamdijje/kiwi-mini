<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function index()
	{
		$this->login->check_admin_login();
		
		$data['title']		=	'Settings';
		$data['heading']	=	'Settings';
		$data['icon']		=	'fa fa-sliders';
		$this->load->view('admin/includes/header',$data);
		$this->load->view('admin/settings',$data);
		$this->load->view('admin/includes/footer',$data);
	}
}
