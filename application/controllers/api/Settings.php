<?php 
require APPPATH.'libraries/REST_Controller.php';
Class Settings extends REST_controller{
		
		function __construct(){
			parent::__construct();
		}
		
		function coordinator_get(){
			$this->load->model('api/api_login_model');
			$check	=	$this->api_login_model->check_login();
			if($check){
				$dDetail			=	$this->lib->get_row_array('coordinators',array('id'=>$check['id']));
				if(!$dDetail){
					$msg	=	array(
					'message'	=>	'Coordinator data not found',
					'status'		=>	'FALSE',
					'time'			=>	time(),
					'c_detail'	=>	NULL
					);
					
					$header	=	404;
					$this->response($msg,$header);
				}
				unset($dDetail->password);
				$msg	=	array(
					'message'	=>	'user logged in',
					'status'		=>	'TRUE',
					'time'			=>	time(),
					'c_detail'	=>	$dDetail
				);
				$header	=	200;
				$this->response($msg,$header);
			
			
			}else{
				$msg	=	array(
					'message'	=>	'User not logged in',
					'status'		=>	'FALSE',
					'time'			=>	time(),
					'c_detail'	=>	FALSE
				);
				$header	=	401;
				$this->response($msg,$header);
			}
			
		}
		
		function save_coordinator_post(){
			$data	=	$this->input->post();
			$this->load->model('api/api_login_model');
			$check	=	$this->api_login_model->check_login();
			if($check){
				if(!$data){
					$msg	=	array(
					'message'	=>	'Empty Response, please fill out all fields',
					'status'		=>	'FALSE',
					'time'			=>	time(),
					'c_detail'	=>	FALSE
				);
				$header	=	403;
				$this->response($msg,$header);
					
				}else{
				
				$id		=	$check['id'];
				$udata	=	array(
					'name'	=>	$data['name'],
					'email'	=>	$data['email'],
					'hospital'	=>	$data['hospital'],
					'phone'	=>	$data['phone']
				);
				
				foreach($udata as $key=>$value)
				{
					if(is_null($value) || $value == '')
						unset($udata[$key]);
				}
				
				
				
				$update	=	$this->lib->replace('coordinators',$udata,'id',$id);
				if($update){
					$check	=	$newData	=	$this->lib->get_row_array('coordinators',array('id'=>$id));
					$session	=	array(
					'name'		=>	$check->name,
					'email'		=>	$check->email,
					'id'				=>	$check->id,
					'login_at'	=>	time(),
					'is_login'		=>	TRUE
					);
					$update_session	=	$this->session->set_userdata('coordinator',$session);
					if(!$update_session){
						$msg	=	array(
						'message'	=>	'Data updated successfully, but failed to update session',
						'status'		=>	'TRUE',
						'time'			=>	time(),
						'c_detail'	=>	$session
						);
						$header	=	200;
						$this->response($msg,$header);
					}
						
						
					$check	=	$session;
						$msg	=	array(
						'message'	=>	'Data updated successfully',
						'status'		=>	'TRUE',
						'time'			=>	time(),
						'c_detail'	=>	$check
						);
					$header	=	200;
				}else{
					$msg	=	array(
					'message'	=>	'Error in updating data',
					'status'		=>	'FALSE',
					'time'			=>	time(),
					'c_detail'	=>	$check
					);
					$header	=	503;
					
				}
				
				$this->response($msg,$header);
				}
				
			}else{
				$msg	=	array(
					'message'	=>	'User not logged in',
					'status'		=>	'FALSE',
					'time'			=>	time(),
					'c_detail'	=>	FALSE
				);
				$header	=	401;
				$this->response($msg,$header);
			}
			
			
		}
		
}