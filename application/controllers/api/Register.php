<?php 
require APPPATH.'libraries/REST_Controller.php';
Class Register extends REST_controller{
		
		function __construct(){
			parent::__construct();
		} 
	
	function student_get(){
	
			$id	=	$this->uri->segment(4);
			$student	=	array(
				'1'	=>	array('fname'=>'Dheeraj','lname'=>'jha'),
				'2'	=>	array('fname'=>'Happy','lname'=>'singh')
			);
			
			if(isset($student[$id])){
			$this->response(array('status'=>'success','message'=>$student[$id]));	
			}else{
			$this->response(array('status'=>'failure','message'=>'Requested student details could not be found'),REST_Controller::HTTP_NOT_FOUND);
			}
			
	}
	
	function speciality_get(){
		$all_spl	=	$this->lib->get_table('speciality',array('name'=>'asc'));
		$header	=	200;
		
		if(isset($all_spl)){
			foreach($all_spl as $spl){
			if($spl->status!=1){
			continue;	
			}
				$speciality[]	=	array(
					'name'	=>	$spl->name,
					'id'			=>	$spl->id,
					'status'	=>	$spl->status
				);
			}
			
			if(!empty($speciality)){
				$msg	=	array(
					'message'	=>	$speciality,
					'status'		=>	'TRUE',
					'time'			=>	time()
				);
				
			}else{
			log_message('error','Empty record found in speciality table');
				$msg	=	array(
					'message'	=>	'No record found',
					'status'		=>	'FALSE',
					'time'			=>	time()
				);
				
				$header	=	404;
				
			}
			
			
			
		}else{
			log_message('error','No record found in speciality table');
			$msg	=	array(
					'message'	=>	'Server Error',
					'status'		=>	'FALSE',
					'time'			=>	time()
				);
				
				$header	=	404;
		}
		
		$this->response($msg,$header);
		
		
	}
	
	function spl_clinic_get($id=NULL){
		$header	=	200;
		if($id==NULL){
			$header	=	404;
			$msg		=	array(
				'message'	=>	'No request found',
				'status'		=>	'FALSE',
				'time'			=>	time()
			);
			
			$this->response($msg,$header);
		}
		
		$clinic	=	$this->lib->get_by_id('spl_clinic','spl_id',$id);
		if($clinic){
			foreach($clinic as $cd){
				$clinic_data	=	$this->lib->get_row_array('clinic',array('id'=>$cd->clinic_id));
				$spl_clinic['clinic'][]	=	array(
					'id'			=>	$clinic_data->id,
					'name'	=>	$clinic_data->clinic_name,
					'status'	=>	$clinic_data->status,
					'city'		=>	$clinic_data->city,
					'lat'		=>	$clinic_data->geo_lat,
					'lng'		=>	$clinic_data->geo_long,
					'phone'	=>	$clinic_data->phone
				);
				
			}
			
			$msg		=	array(
				'message'	=>	$spl_clinic,
				'status'		=>	'TRUE',
				'time'			=>	time()
			);
			
			
			
		}else{
			$header	=	404;
			$msg		=	array(
				'message'	=>	'No record found',
				'status'		=>	'FALSE',
				'time'			=>	time()
			);
			
			$this->response($msg,$header);
		}
		
		
		
		$this->response($msg,$header);
	}
	
	public function schedule_date_get($cid,$sid){
	$dates =	array();
	$this->load->model('api/api_login_model');
	$header	=	200;
		if(!$cid OR !$sid){
			$header	=	404;
			$msg		=	array(
				'message'	=>	'Empty Clinic or Speciality, please provide Clinic id and speciality id',
				'status'		=>	'FALSE',
				'time'			=>	time()
			);
			$this->response($msg,$header);
			
		}
		
		$spl_clinic			=	$this->lib->get_row_array('spl_clinic',array('clinic_id'=>$cid,'spl_id'=>$sid));
		if($spl_clinic){
			$start	=	"+ ".$spl_clinic->minimum_notice." minute";
			$end		=	"+ ".$spl_clinic->maximum_notice." day";
			$startDate	= date("d-m-y", strtotime($start));
			$endDate	= date("d-m-y", strtotime($end));
			
			$dates	=	array();
			while(strtotime($start) <= strtotime($end)) {
				$temp_date	=	date('d-m-y',strtotime($start));
				
				$check_empty	=	$this->api_login_model->filter_empty_dates($start,$spl_clinic->id);
				if($check_empty){
					$dates[]		=	$temp_date;
				}else{
				log_message('error','Removing date :'.$temp_date);
				}
				
				
                $start = date("Y-m-d", strtotime("+1 day", strtotime($start)));
			}
			
			
			
			$msg		=	array(
					'dates'	=>	$dates,
					'cs_id'	=>	$spl_clinic->id,
					'status'	=>	'TRUE',
					'time'		=>	time()
				);
			

			
			$this->response($msg,$header);
			
		}else{
			$header	=	404;
				$msg		=	array(
					'message'	=>	'No Record found for this speciality and clinic combination',
					'status'		=>	'FALSE',
					'time'			=>	time()
				);
				$this->response($msg,$header);
		}
		
		
		
	}
	
	
	public function schedule_time_get($date,$csid){
		$this->load->model('api/api_login_model');
		
		$clinic_spl	=	$this->lib->get_row_array('spl_clinic',array('id'=>$csid));
		$hours		=	array();
		$header	=	200;
		if(!$date OR !$date){
			$header	=	404;
			$msg		=	array(
				'message'	=>	'Empty request, expacting date and clinic schedule id as well',
				'status'		=>	'FALSE',
				'time'			=>	time()
			);
		
			$this->response($msg,$header);
			exit();
		
		}
		
		$date_ar	=	explode('-',$date);
		
		
		$cs_data	=	$this->lib->get_row_array('clinic_schedule',array('cs_id'=>$csid));
		
		$day			=	date('D',strtotime($date_ar[0].'-'.$date_ar[1].'-20'.$date_ar[2]));
		switch($day){
			case "Sun":
				if($cs_data->sun_hour){
					$hours	=	explode(',',$cs_data->sun_hour);
					break;
				}
				
			case	"Mon":
				if($cs_data->mon_hour){
					$hours	=	explode(',',$cs_data->mon_hour);
				break;
				}
				
			case "Tue":
				if($cs_data->tue_hour){
				$hours	=	explode(',',$cs_data->tue_hour);
				break;
				}
				
			case "Wed":
				if($cs_data->wed_hour){
				$hours	=	explode(',',$cs_data->wed_hour);
				break;
				}
				
			case "Thu":
			if(isset($cs_data->thu_hour)){
				$hours	=	explode(',',$cs_data->thu_hour);
				break;
				}
				
			case "Fri":
				if(isset($cs_data->fri_hour)){
				$hours	=	explode(',',$cs_data->fri_hour);
				break;
				}
				
				
			case	"Sat":
				if(isset($cs_data->sat_hour)){
				$hours	=	explode(',',$cs_data->sat_hour);
				break;
				}
				
			case "default":
				$hours	=	"";
		}
		
		if(is_array($hours)){
		log_message('error','hours array is:::::'.implode(',',$hours));
		foreach($hours as $key=>$time){
			$checkBooking	=	$this->lib->get_multi_where('bookings',array('cs_id'=>$csid,'sch_date'=>$date,'sch_time'=>$time,'status'=>1));
			if($checkBooking){
				log_message('error','Unsetting '.$hours[$key]);
				unset($hours[$key]);
			}
			
			
		}
		log_message('error','yeahhh date: '.$date.' hours is '.implode(',',$hours).' c_s id is'.$csid);
		}else{
		echo "No hours";
		}
		
		
		
		
		
		$hours	=	$this->api_login_model->check_block_time($date,$hours,$csid);
		
		
		if(!empty($hours)){
			log_message('error','value we have is'.implode('--',$hours));
			$msg	=	array(
				'hours'	=>	$hours,
				'date'		=>	$date,
				'cs_id'	=>	$csid,
				'status'	=>	TRUE,
				'time'		=>	time()
		
			);
		}else{
		$header	=	404;
		log_message('error','No request value found');
			$msg	=	array(
				'msg'		=>	'Sorry, no empty timeslots available, please select another date',
				'hours'	=>	0,
				'date'		=>	$date,
				'cs_id'	=>	$csid,
				'status'	=>	FALSE,
				'time'		=>	time()
		
			);
		
		}
	
		
		$this->response($msg,$header);	
		exit();
	}
	
	public function check_date_get($date,$cs_id){
		$this->load->model('api/api_login_model');
		$msg=$this->api_login_model->filter_empty_dates($date,$cs_id);
		if($msg){
		$msg='yes';	
		}else{
		$msg='no';
		}
		$this->response($msg);	
	}
	
	public function save_booking_post(){
		$header	=	200;
		$data	=	$this->input->post();
		
		$data['added_on']	=	time();
		$data['added_by']	=	1;
		
		
		// Checking booking status again
		$checkBooking	=	$this->lib->get_multi_where('bookings',array('cs_id'=>$data['cs_id'],'sch_date'=>$data['sch_date'],'sch_time'=>$data['sch_time']));
		if($checkBooking){
			$msg	=	array(
			'msg'		=>	'Sorry, The appointment you tried to schedule was taken just few second ago, Please choose another date and time',
			'status'	=>	TRUE,
			'time'		=>	time()
		
			);
			
			$header	=	403;
			$this->response($msg,$header);	
			exit();
		}
		
		$insert	=	$this->db->insert('bookings',$data);
		if($insert){
				$bid		=	$this->db->insert_id();
			$cs_data	=	$this->lib->get_row_array('spl_clinic',array('id'=>$data['cs_id']));
			$clinicName	=	$this->lib->get_row('clinic','id',$cs_data->clinic_id,'clinic_name');
			
			// Mail
			$cs_detail					=	$this->lib->get_row_array('spl_clinic',array('id'=>$data['cs_id']));
				$clinic_data			=	$this->lib->get_row_array('clinic',array('id'=>$cs_detail->clinic_id));
				$spl_data				=	$this->lib->get_row_array('speciality',array('id'=>$cs_detail->spl_id));
				
			// Mail to user 
				$mdata['name']		=	"Appoinment scheduler";
				$mdata['from']			=	$this->lib->get_settings('email');
				$mdata['to']			=	$data['p_email'];
				$mdata['message']	=	"Hi ".$data['first_name']."<br>
				Your appoinment with <b>".$clinic_data->clinic_name."</b> for <b>".$spl_data->name."</b> dated : <b>".$data['sch_date']."</b> on time <b>".$data['sch_time']."</b> is scheduled successfully. <br>Please see confirmation message below.<br>
				<hr>".$cs_data->confirmation_message."<br><hr>
				<i>If you want to cancel appoinment, <a href='".base_url('cancel/booking/'.base64_encode($bid).'/'.base64_encode($data['p_email']))."'><b>Please click here</b></a></i>
				
				<br>Thanks and regards<br>Admin team";
				$mdata['subject']		=	"Appoinment booked successfully : ".$this->lib->get_settings('sitename');
				$email_send			=	$this->lib->send_formatted_mail($mdata);
				
				
				
			// Mail to scheduler
				$mdata['to']			=	$cs_data->scheduler_email;
				$mdata['message']	=	"Hi<br>
				A new appoinment with <b>".$clinic_data->clinic_name."</b> for <b>".$spl_data->name."</b> dated : <b>".$data['sch_date']."</b> on time <b>".$data['sch_time']."</b> is scheduled. <br>Please see patent details below.<br>
				<hr>
				Name : ".$data['first_name']." ".$data['last_name']."<br>
				Email : ".$data['p_email']."<br>
				Phone: ".$data['p_phone']."<br>
				Notice: ".$data['booking_notice']."<br>
				
				<br><hr>
				
				<br>Thanks and regards<br>Admin team";
				$mdata['subject']		=	"New Booking notification : ".$this->lib->get_settings('sitename');
				$email_send			=	$this->lib->send_formatted_mail($mdata);
				
			// Mail ends
			
			$msg	=	array(
			'msg'					=>	'Your Appointment with '.$clinicName.' is confirmed on '.$data['sch_date'].' on '.$data['sch_time'],
			'booking_date'		=>	$data['sch_date'],
			'booking_time'		=>	$data['sch_time'],
			'status'				=>	TRUE,
			'time'					=>	time()
		
			);
		}else{
			$header	=	503;
			$msg	=	array(
			'msg'					=>	'Unable to complete booking, please try again latter',
			'status'				=>	FALSE,
			'time'					=>	time()
		
			);
		
		}
		
		$this->response($msg,$header);	
		exit();
		
		
		
	}
}