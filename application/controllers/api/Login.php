<?php 
require APPPATH.'libraries/REST_Controller.php';
Class Login extends REST_controller{
		
		function __construct(){
			parent::__construct();
		}
		
		function validate_post(){
			$data	=	$this->input->post();
			if(!$data['email'] OR !$data['password']){
			$msg	=	array(
					'message'	=>	'Empty email/password',
					'status'		=>	'FALSE',
					'time'			=>	time()
				);
				
				$header	=	403;	
				$this->response($msg,$header);
			}
			
			$check_login	=	$this->lib->get_row_array('coordinators',array('email'=>$data['email'],'password'=>sha1($data['password'])));
			if($check_login){
				$session	=	array(
				'name'		=>	$check_login->name,
				'email'		=>	$check_login->email,
				'id'				=>	$check_login->id,
				'login_at'	=>	time(),
				'is_login'		=>	TRUE
			);
			$this->session->set_userdata('coordinator',$session);
			
			$coordinator	=	$this->session->coordinator;
			
			
				$msg	=	array(
					'message'		=>	'Login Success',
					'coordinator'	=>	$coordinator,
					'status'			=>	'TRUE',
					'time'				=>	time()
				);
				$header	=	200;
			}else{
				$msg	=	array(
					'message'	=>	'Incorrect Email/password',
					'status'		=>	'FALSE',
					'time'			=>	time()
				);
				$header	=	401;
			
			}
			
			
			$this->response($msg,$header);
			
		}
		
		/*
		This will logout from system and will return TRUE if successfully logged out	
		*/
		
		function logout_get(){
			$this->session->sess_destroy();
			$check_login	=	$this->session->coordinator;
			if(!$check_login){
				$msg	=	array(
					'message'	=>	'Logged out successfully',
					'status'		=>	'TRUE',
					'time'			=>	time()
				);
				$header	=	200;
				$this->response($msg,$header);
			}else{
				$msg	=	array(
					'message'	=>	'Something went wrong, please try again',
					'status'		=>	'FALSE',
					'time'			=>	time()
				);
				$header	=	503;
				$this->response($msg,$header);
			
			}
			
			
		}
		
}