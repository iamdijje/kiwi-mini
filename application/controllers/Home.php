<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		
		
		$data['title']			=	'Dashboard';
		$data['heading']	=	'Dashboard';
		$data['icon']		=	'fa fa-dashboard';
		$this->load->view('front/includes/header',$data);
		$this->load->view('front/includes/sidebar',$data);
		$this->load->view('front/default',$data);
		$this->load->view('front/includes/footer',$data);
	}
	
	
	function readExcel()
	{
        $this->load->library('csvreadernew');
        $result =   $this->csvreadernew->parse_file('static/inventrycsv.csv');

        $data['csvData'] =  $result;
		foreach($data['csvData'] as $rdata){
			if($rdata['SKU_Product_Id']==''){
			continue;	
			}
			echo "<pre>";
			print_r($rdata);
			echo "</pre>";
		}
	}
}

