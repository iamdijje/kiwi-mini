<div class="row">
<!--
	End of first col
	-->
<div class="col-lg-4">
	<div class="panel panel-default col-lg-12">
	<h3><i class="fa fa-info-circle"></i> Product Information</h3>
	<hr>
	<dl class="dl-horizontal">
		<dt>SKU ID</dt>
		<dd><?php echo $prod_info->SKU_Product_Id;?></dd>
		
		<dt>Product Name</dt>
		<dd><?php echo $prod_info->product_name	;?></dd>
		
		<dt>Product Category</dt>
		<dd><?php echo $prod_info->category	;?></dd>
		
		<dt>Product Barcode</dt>
		<dd><?php echo $prod_info->barcode_no	;?></dd>	
		
	
		<dt>Product Cost</dt>
		<dd>&#36;<?php echo $prod_info->cost_price	;?></dd>	
		
		<dt>Product Selling price</dt>
		<dd>&#36;<?php echo $prod_info->selling_price	;?></dd>	
		
		<dt>Profit</dt>
		<dd>&#36;<?php echo $prod_info->profit	;?></dd>	
		
		<dt>Stock IN</dt>
		<dd><?php echo $prod_info->stock_in	;?></dd>
		
		<dt>Stock out</dt>
		<dd><?php echo $prod_info->stock_out	;?></dd>
	</dl>
	</div>
	<br>
	<div class="panel panel-default col-lg-12">
		<h3><i class="fa fa-globe"></i> Sells Info</h3>
		<h5>For last <?php echo $this->lib->get_settings('avg_sales_day');?> day/s</h5>
		<dl class="dl-horizontal">
		<dt>Total sales</dt>
		<dd>&#36;<?php echo $sales_info->total_sale;?></dd>
		
		<dt>Average Sales</dt>
		<dd>&#36;<?php echo $sales_info->avg_sales;?></dd>
		
		<dt>Total Profit</dt>
		<dd>&#36;<?php echo $sales_info->total_profit;?></dd>
		
		<dt>Average Profit</dt>
		<dd>&#36;<?php echo $sales_info->avg_profit;?></dd>
		
		<dt>Total product sale</dt>
		<dd><?php echo $sales_info->total_stock_sale;?></dd>
		
		
	</dl>
		
	</div>
</div>

<!--
	End of first col
	-->
<div class="col-lg-4">
	
	<div class="panel panel-default col-lg-12">
		<h3><i class="fa fa-user"></i> Seller information</h3>
		<dl class="dl-horizontal">
		<dt>Seller Name</dt>
		<dd><?php echo $prod_info->seller_name;?></dd>
		
		<dt>Seller Phone/Mobile</dt>
		<dd><?php echo $prod_info->seller_phone;?></dd>
		
		<dt>Seller address</dt>
		<dd><?php echo $prod_info->seller_address;?></dd>
		
		
		</dl>
		
	</div>
	
	<div class="panel panel-default col-lg-12">
		<h3><i class="fa fa-home"></i> Retailer Information</h3>
		<dl class="dl-horizontal">
		<dt>Retailer Name</dt>
		<dd><?php echo $prod_info->retail_name;?></dd>
		
		<dt>Retailer Phone</dt>
		<dd><?php echo $prod_info->retailer_phone;?></dd>
		
		<dt>Retailer Email</dt>
		<dd><?php echo $prod_info->retailer_email;?></dd>
		
		
		<dt>Retailer Address</dt>
		<dd><?php echo $prod_info->retailer_address;?></dd>
		
		<dt>Retailer Cost</dt>
		<dd>&#36;<?php echo $prod_info->retailer_cost;?></dd>
		
		<dt>Retailer Selling cost</dt>
		<dd>&#36;<?php echo $prod_info->retailor_selling;?></dd>
		
	</dl>
		
	</div>
	
	<div class="panel panel-default col-lg-12">
		<h3><i class="fa fa-eye"></i> Other information</h3>
		<dl class="dl-horizontal">
		<dt>Turnover</dt>
		<dd><?php echo $prod_info->turnover;?></dd>
		
		<dt>Seller Phone/Mobile</dt>
		<dd><?php echo $prod_info->seller_phone;?></dd>
		
		<dt>Seller address</dt>
		<dd><?php echo $prod_info->seller_address;?></dd>
		
		
		</dl>
		
	</div>
	
	
</div>

<div class="col-lg-4">
	<div class="panel panel-default col-lg-12">
		<h3><i class="fa fa-user"></i> Customer information</h3>
		<dl>
		<dt>Customer Name</dt>
		<dd><?php echo $prod_info->customer_name;?></dd>
		
		<dt>Customer Phone</dt>
		<dd><?php echo $prod_info->seller_phone;?></dd>
		
		<dt>Customer Email</dt>
		<dd><?php echo $prod_info->customer_email;?></dd>
		
		<dt>Customer Address</dt>
		<dd><?php echo $prod_info->customer_address;?></dd>
		
		<dt>Date of sale</dt>
		<dd><?php echo $prod_info->date_of_sale;?></dd>
		
		</dl>
		
	</div>
	
	<div class="panel panel-default col-lg-12">
		<h3><i class="fa fa-globe"></i> Selling site</h3>
		<dl class="dl-horizontal">
		<dt>Selling site</dt>
		<dd><?php echo $prod_info->selling_site;?></dd>
		
		<dt>Listing fee</dt>
		<dd>&#36;<?php echo $prod_info->listing_fees;?></dd>
		
		
	</dl>
		
	</div>
</div>
</div>