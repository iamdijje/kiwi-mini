                    <ul class="nav" id="side-menu">
                       
						 <li>
                            <a href="<?php echo base_url('products')?>"><i class="fa fa-cube fa-fw"></i> Products</a>
                        </li>
                     
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="<?php if(isset($icon)){ echo $icon;}?> fa-fw"></i> <?php if(isset($heading)){ echo $heading;}?></h1>
                </div>
				<div class="clearfix"></div>
                <!-- /.col-lg-12 -->
				<?php 
					
				$this->lib->alert_message();	
				?>
				
				<div class="clearfix"></div>
            </div>