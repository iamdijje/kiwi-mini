
            <!-- /.row -->
			<div class="row input-field">
					
						
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
							<button data-toggle="modal" data-target="#myModal" onclick="$('#loadData').load('<?php echo base_url('products/import/')?>')" type="button" class="btn btn-default col-xs-7"><i class="fa fa-upload"></i> Import CSV </button>
							<a href="<?php echo base_url('products/export')?>"><button class="btn btn-info col-xs-4 col-xs-push-1"><i class="fa fa-download"></i> Export</button></a>
						</div>
        </div>
		<div class="row">
			<div class="panel panel-default">
				<table class="table table-bordered paginated-table">
				  <thead>
					<tr>
					  <th>Sr no</th>
					  <th>Product date</th>
					  <th>Product Name</th>
						<th>Product barcode</th>
					  <th>Product SKU</th>
					  <th>Product Stock</th>
					  
					 
					</tr>
				  </thead>
				  <tbody>
				  <?php 
					if($products){
					$i=1;
						foreach($products as $prod_data){
							?>
							 <tr>
						<th><?php echo $i;?></th>
					 
					
					  <td><?php echo date('d-M-Y',$prod_data->product_ts);?>
						  <br><small><i class="fa fa-calendar"></i> <?php echo date('d-M-Y',$prod_data->product_added_on);?></small>
						  </td>
					 
					
					  <td><?php echo $prod_data->product_name;?></td>
					  <td><?php echo $prod_data->product_barcode;?></td>
					  <td><?php echo $prod_data->product_sku;?></td>
					  <td><?php echo $prod_data->product_stock;?></td>
					  
					  
					 
					</tr>
					<?php
							$i++;
						}
					}
					?>
					 
					
				  </tbody>
				</table>
				
				
			</div>
			<div class="clearfix"></div>
				 <span class="prev btn btn-rounded btn-default"><i class="fa fa-angle-double-left"></i> Previous</span> <span class="next  btn btn-rounded btn-default"><i class="fa fa-angle-double-right"></i> Next</span>
				 <div class="clearfix"></div>
		</div>
        <!-- /#page-wrapper -->
	<script>
		var maxRows = 10;
$('.paginated-table').each(function() {
    var cTable = $(this);
    var cRows = cTable.find('tr:gt(0)');
    var cRowCount = cRows.size();
    
    if (cRowCount < maxRows) {
        return;
    }

    cRows.each(function(i) {
        $(this).find('td:first').text(function(j, val) {
          // return (i + 1) + " - " + val;
        }); 
    });

    cRows.filter(':gt(' + (maxRows - 1) + ')').hide();


    var cPrev = cTable.siblings('.prev');
    var cNext = cTable.siblings('.next');

    cPrev.addClass('disabled');

    cPrev.click(function() {
        var cFirstVisible = cRows.index(cRows.filter(':visible'));
        
        if (cPrev.hasClass('disabled')) {
            return false;
        }
        
        cRows.hide();
        if (cFirstVisible - maxRows - 1 > 0) {
            cRows.filter(':lt(' + cFirstVisible + '):gt(' + (cFirstVisible - maxRows - 1) + ')').show();
        } else {
            cRows.filter(':lt(' + cFirstVisible + ')').show();
        }

        if (cFirstVisible - maxRows <= 0) {
            cPrev.addClass('disabled');
        }
        
        cNext.removeClass('disabled');

        return false;
    });

    cNext.click(function() {
        var cFirstVisible = cRows.index(cRows.filter(':visible'));
        
        if (cNext.hasClass('disabled')) {
            return false;
        }
        
        cRows.hide();
        cRows.filter(':lt(' + (cFirstVisible +2 * maxRows) + '):gt(' + (cFirstVisible + maxRows - 1) + ')').show();

        if (cFirstVisible + 2 * maxRows >= cRows.size()) {
            cNext.addClass('disabled');
        }
        
        cPrev.removeClass('disabled');

        return false;
    });

});
</script>