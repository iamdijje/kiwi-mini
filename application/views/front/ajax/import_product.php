<form class="form" method="post" action="<?php echo base_url('products/save');?>" enctype="multipart/form-data">
	<div class="form-group">
		<label>Select File</label>
		<input type="file" name="csvProduct" class="hidden" accept=".csv" id="csvSelect">
		<span class="btn btn-default" onclick="$('#csvSelect').click();"><i class="fa fa-upload"></i> Choose CSV file of product<span>
	</div>
	
	<div class="form-group">
		<button class="btn btn-success"><i class="fa fa-save"></i> Save Products</button>
	</div>
</form>