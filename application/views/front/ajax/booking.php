<?php 
	if($dates){	
	?>
	<div class="form-group">
		<label class="col-lg-4">Select Date</label>
		<div class="col-lg-8">
			<select class="form-control" name="sch_date" id="date">
				<?php 
					
					foreach($dates as $key=>$value){
					?>	
					<option value="<?php echo $value?>"><?php echo $value;?></option>
					<?php
					}
					
				?>
			</select>
			<input type="hidden" name="cs_id" id="cs_id" value="<?php echo $cs_id?>">
		</div>
		
		
	</div>
	
	<div class="form-group">
		<label class="col-lg-4">Select time</label>
		<div class="col-lg-8">
			<select class="form-control" name="sch_time" id="time" onchange="$('#booking_form').show()">
				
			</select>
			
		</div>
		
		
	</div>
	<div id="booking_form">
		<div class="form-group">
			<label class="col-lg-4">First name</label>
			<div class="col-lg-8">
				<input type="text" placeholder="First name" class="form-control" name="first_name" required>
			</div>
			
			
		</div>
		
		<div class="form-group">
			<label class="col-lg-4">Last name</label>
			<div class="col-lg-8">
				<input type="text" placeholder="Last name" class="form-control" name="last_name" >
			</div>
			
			
		</div>
		
		<div class="form-group">
			<label class="col-lg-4">Patient Email address</label>
			<div class="col-lg-8">
				<input type="email" placeholder="Email address" name="p_email" class="form-control" required>
			</div>
			
			
		</div>
		
		<div class="form-group">
			<label class="col-lg-4">Patient Phone</label>
			<div class="col-lg-8">
				<input type="text" placeholder="Phone number" name="p_phone" class="form-control" required>
			</div>
			
			
		</div>
		
		<div class="form-group">
			<label class="col-lg-4">Booking Note</label>
			<div class="col-lg-8">
				<textarea class="form-control" name="booking_notice"></textarea>
			</div>
			
			
		</div>
		
		<div class="form-group">
			<label class="col-lg-4"></label>
			<div class="col-lg-8">
				<button class="btn btn-primary"><i class="fa fa-save"></i> Save booking</button>
			</div>
			
			
		</div>
	</div>
	
	
	<?php 
		}else{
	?>
	<div class="text-center alert alert-warning" id="alert">
		<h2 class=""><i class="fa fa-warning"></i> No Dates right now</h2>
		<p>Please select another clinic or try again latter</p>
	</div>
	<?php
	}	
?>

<script>
	$('#booking_form').hide();
	
	
	$(document).ready(function() {
		
		$('#date').change(function () {
			var date = $('#date').val();
			var cs_data = $('#cs_id').val();
			var data='date='+date+'&cs_id='+cs_data;
			$.ajax({
				type:"POST",
				url:"<?php echo base_url('home/get_time')?>",
				data:data,
				success:function(html) {
					$("#time").html(html);
				}
			});
			return false;
		});
		
		
		
		
		
	});
</script>