<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('static/front/')?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('static/front/')?>/css/font-awesome.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><?php echo $this->lib->get_settings('sitename')?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
                <h1>Book Your appointment</h1>
                <p class="lead">Complete with pre-defined file paths that you won't have to change!</p>
				<?php 
				$this->lib->alert_message();	
				?>
                <form class="form-horizontal">
					<div class="form-group">
						<label class="col-lg-4">Speciality</label>
						<div class="col-lg-8">
							<select class="form-control" id="speciality">
								<option selected disabled>Selected</option>
								<?php
								if($speciality){
								foreach($speciality as $sd){
								?>	
								<option value="<?php echo $sd->id?>"><?php echo $sd->name?></option>
								<?php
								}	
									
								}	
								?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4">Select Clinic</label>
						<div class="col-lg-8">
							<select class="form-control" id="clinic">
								<option selected disabled>Selected</option>
							</select>
							
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-lg-4"></label>
						<div class="col-lg-8">
							<button class="btn btn-success" id="schedule"><i class="fa fa-arrow-right"></i> Schedule</button>
						</div>
					</div>
					
					<div id="booking">
						<p class="text-center">Please wait..</p>
					</div>
					
					
				</form>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo base_url('static/front/')?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('static/front/')?>/js/bootstrap.min.js"></script>
	<script>
	$('#booking').hide();
	$(document).ready(function() {
	
	$('#speciality').change(function () {
	$('#alert').hide();
	var spl = $('#speciality').val();
	var data='spl='+spl;
	$.ajax({
	type:"POST",
	url:"<?php echo base_url('home/get_clinic')?>",
	data:data,
	success:function(html) {
	$("#clinic").html(html);
	}
	});
	return false;
	});
	
	$('#schedule').click(function () {
	$('#booking').show();
	var clinic = $('#clinic').val();
	var data='clinic='+clinic;
	$.ajax({
	type:"POST",
	url:"<?php echo base_url('home/get_schedule')?>",
	data:data,
	success:function(html) {
	$("#booking").html(html);
	}
	});
	return false;
	});
	
	
	
	
	});
	
	
	
	
	</script>

</body>

</html>
