<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	
	
	public function generate_promo(){
		$this->load->helper('string');
		
		
		$new_promo				=	random_string('alpha',6);
		$check_promot			=	$this->lib->get_row_array('users',array('user_promocode'=>$new_promo));
		if($check_promot){
			$this->generate_promo();
		}
		
		return strtoupper($new_promo);
		
	}
	
	
	public function save_user_login($user_id){
		
		if(!$user_id){
		return false;	
		}
		$user_row							=	$this->lib->get_row_array('users',array('user_id'=>$user_id));
		// Generate Session
		if(!$user_row){
		return false;	
		}
		
		$user_sesson['user_id']		=	$user_row->user_id;
		$user_sesson['name']		=	$user_row->user_name;
		$user_sesson['email']		=	$user_row->user_email;
		$user_sesson['phone']		=	$user_row->user_phone;
		$user_sesson['paid']			=	$user_row->user_membership;
		$user_sesson['login_at']	=	time();
		$user_sesson['login']			=	TRUE;
		
		$this->session->set_userdata('users',$user_sesson);
		$update	=	$this->lib->update('users',array('user_last_login'=>time()),'user_id',$user_id);
		if($update){
			return TRUE;	
		}
		
		
	}
	
	public function check_user_login(){
		$user_info	=	$this->session->userdata('users');
		if(!$user_info['name'] OR !$user_info['login'] OR !$user_info['user_id']){
			$this->lib->redirect_msg('You need to login to proceed','warning','login');
		}
	}
	
	public function giftcard_eligblity($user_id){
		/*
		*	Find promo code of user
		*	check if any other user signup by same code
		*	if yes, eligible else not
		*/
		
		if(!$user_id){
			return FALSE;
		}
		
		$user_info	=	$this->lib->get_row_array('users',array('user_id'=>$user_id));
		if($user_info){
			$promo	=	$user_info->user_promocode;
			
			//check if anyone used this promo code
			$check	=	$this->lib->get_by_id('users','ref_code',$promo);
			if($check){
				return TRUE;	
			}else{
				return FALSE;		
			}
		}else{
			return false;
		}
	}
	
	public function user_details($user_id){
		if(!$user_id){
			return false;
		}
		
		$this->db->select('*');
		$this->db->from('users		user');
		$this->db->join('favourites			fvr8',	'fvr8.user	=	user.user_id','left');
		$this->db->join('giftcard_promo		gift',	'gift.user_id	=	user.user_id','left');
		$this->db->join('country				country',	'country.country_id	=	user.user_country','left');
		
		$this->db->where('user.user_id',$user_id);
		$query	=	$this->db->get();
		if($query AND $query->num_rows()>0){
			return $query->row();
		}else{
			return false;
		}
		
		
	}
	
	public function refresh_auth(){
	
		$userAuthString	=	$_SERVER['HTTP_'.strtoupper($this->lib->get_settings('device_auth_key'))];
		if(!$userAuthString){
		log_message('error','User Login auth not defined');
		return false;	
		}
		$userAuthArray	=	explode('__',$userAuthString);
		
		$email				=	base64_decode($userAuthArray[0]);
		$userId				=	base64_decode($userAuthArray[1]);
		
		$checkUserInfo	=	$this->lib->get_row_array('users',array('user_id'=>$userId,'user_email'=>$email));
		if($checkUserInfo){
			log_message('error','User login refreshed');
			return $this->save_user_login($userId);
		}
	}

}