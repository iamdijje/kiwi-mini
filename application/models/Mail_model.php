<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail_model extends CI_Model {
		
	public function add_scheduer($email,$csid){
		if(!$email){
		return FALSE;	
		}
				$ins['email']			=	$email;
				$ins['status']		=	1;
				$ins['password']	=	mt_rand();
				
				$save_scheduler	=	$this->db->insert('schedulers',$ins);
				if($save_scheduler){
				
				$mdata['name']		=	"Appoinment scheduler";
				$mdata['from']			=	$this->lib->get_settings('email');
				$mdata['to']			=	$ins['email'];
				$mdata['message']	=	"Hi <br>
				You are added as scheduler for appintment scheduler.<br>
				Your login details are:<br>
				Email : ".$ins['email']."<br>
				Password: ".$ins['password']."<br>
				Login at : ".base_url('clinic/login')."
				<br>Thanks and regards<br>Admin team";
				$mdata['subject']		=	"Scheduler account created : ".$this->lib->get_settings('sitename');
				$email_send			=	$this->lib->send_formatted_mail($mdata);
				
				}
		
	}
	
	public function check_scheduler_cs($scId,$csId){
		if(!$scId OR !$csId){
		return false;	
		}
		
		$check	=	$this->lib->get_row_array('scheduler_cs',array('scheduler_id'=>$scId,'cs_id'=>$csId));
		if($check){
		return TRUE;	
		}else{
		$insert	=	$this->db->insert('scheduler_cs',array('scheduler_id'=>$scId,'cs_id'=>$csId));
		
		}
		
		
		
	}
	
	public function scheduler_add_mail($email){
				$check					=	$this->lib->get_row_array('schedulers',array('email'=>$email));
				if(!$check){
				return FALSE;	
				}
				
				if(!$email_send){
				log_message('error','message not sent to '.$check->email);	
				}
		
	}
	
}