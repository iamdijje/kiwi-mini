<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

class Api_login_model extends CI_Model {
	function check_login(){
		$data	=	$this->session->coordinator;
		if(!$data AND !$data['is_login']){
		return FALSE;	
		}else{
		return $data;
		}
		
	}
	
	function check_block_time($date,$hours,$csid){
		if(!$date OR !$csid){
		log_message('error','empty value');
		return FALSE;	
		}
	//	log_message('error','date: '.$date.' hours is '.implode(',',$hours).' c_s id is'.$csid);
		
		$csData	=	$this->lib->get_row_array('spl_clinic',array('id'=>$csid));
		if($csData){
				$sc_mail			= $csData->scheduler_email;
				log_message('error',$sc_mail);
				$check_block	=	$this->lib->get_row_array('block_schedule',array('b_date'=>$date,'scheduler'=>$sc_mail));
				
				if(!empty($check_block)){
				
				
						foreach($hours as $key => $value){
						$value		=	str_replace(':','',$value);
						$from_time	=	str_replace(':','',$check_block->from_time);
						$to_time	=	str_replace(':','',$check_block->to_time);
						log_message('error','From time is '.$from_time);
						log_message('error','to time is '.$to_time);
						log_message('error','value time is '.$value);
						
						
						if($value>= $from_time AND $value<=$to_time){
							unset($hours[$key]);
						}
					
					}
				}else{
					log_message('error','No data');
				}
				
				
				// Allowed time
				//$this->db->order_by('id','asc');
				$check_allowed =	$this->db->get_where('allowed_schedule', array('date'=>$date,'scheduler'=>$sc_mail));
				
				if($check_allowed->num_rows()>0){
				log_message('error','Adding allowed time');
					foreach($check_allowed->result() as $ca){
						foreach(explode(',',$ca->time) as $key=>$time){
							$hours[]			=	$time;
						}
						
					}
					
				}else{
				log_message('error','no allowed time');
				}
				if(is_array($hours)){
					log_message('error','Values are '.implode(',',$hours));
				}else{
				log_message('error','Values are empty');
				}
				
				
		}
		if($hours){
			$hours =	array_unique(array_values(array_filter($hours)));
			return array_values($hours);
		}else{
		return FALSE;
		}
		
		
		
	}
	
	
	public function filter_empty_dates($date,$csid){
	
		$cs_data	=	$this->lib->get_row_array('clinic_schedule',array('cs_id'=>$csid));
		
		$day			=	date('D',strtotime($date));
		
		switch($day){
			case "Sun":
				if(isset($cs_data->sun_hour)){
					$hours	=	explode(',',$cs_data->sun_hour);
					break;
				}
				
			case	"Mon":
				if(isset($cs_data->mon_hour)){
					$hours	=	explode(',',$cs_data->mon_hour);
				break;
				}
				
			case "Tue":
				if(isset($cs_data->tue_hour)){
				$hours	=	explode(',',$cs_data->tue_hour);
				break;
				}
				
			case "Wed":
				if(isset($cs_data->wed_hour)){
				$hours	=	explode(',',$cs_data->wed_hour);
				break;
				}
				
			case "Thu":
			if(isset($cs_data->thu_hour)){
				$hours	=	explode(',',$cs_data->thu_hour);
				break;
				}
				
			case "Fri":
				if(isset($cs_data->fri_hour)){
				$hours	=	explode(',',$cs_data->fri_hour);
				break;
				}
				
				
			case	"Sat":
				if(isset($cs_data->sat_hour)){
				$hours	=	explode(',',$cs_data->sat_hour);
				break;
				}
				
			case "default":
				$hours	=	"";
		}
		
		if(!empty($hours)){
		log_message('error','hours array is:'.implode(',',$hours));
		foreach($hours as $key=>$time){
			$checkBooking	=	$this->lib->get_multi_where('bookings',array('cs_id'=>$csid,'sch_date'=>$date,'sch_time'=>$time,'status'=>1));
			if($checkBooking){
				log_message('error','Unsetting '.$hours[$key]);
				unset($hours[$key]);
			}
			
			
		}
		}else{
		
		}
		$hours	=	$this->api_login_model->check_block_time($date,$hours,$csid);
		
		if($hours){
			log_message('error','hours array is now:'.implode(',',$hours));
			return TRUE;
		}else{
		return FALSE;
		}
	}
	

}