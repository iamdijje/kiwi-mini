<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {
		
		public function generate_csv($query,$path='static/front/csv/download/'){
		
		if(!$query){
		return FALSE;	
		}
		$this->load->dbutil();
		$delimiter = ",";
		$newline = "\r\n";
		$enclosure = '"';
		
		$data		=	$this->dbutil->csv_from_result($query,$delimiter, $newline, $enclosure);
		if(!is_dir($path)){
			mkdir($path,655);
		}
		$this->file	=	$path.date('d-m-y',time()).'--'.time().'.csv';
		
		if(file_put_contents ($this->file,$data)){
			
			return $this->file;
			
		}else{
		return FALSE;
		}
		
		
		
	}
	
	public function average_sales($sku_id,$col='selling_price'){
			if(!$sku_id){
			return false;	
			}
			
			$salesDay	=	$this->lib->get_settings('avg_sales_day');
			$this->db->from('products');
			//$query = $this->db->select_sum('selling_price', 'Amount');
			$this->db->select('product_date,product_name,
									AVG(selling_price) AS avg_sales,
									SUM(selling_price) AS total_sale,
									SUM(profit) AS total_profit,
									AVG(profit) AS avg_profit,
									SUM(total_stock) AS total_stock_sale
									
									',
									
									FALSE);
			$this->db->where('SKU_Product_Id',$sku_id);
			$this->db->where('sell_date_ts>',strtotime("12:00:00")-(3600*24*$salesDay));
			$query=	$this->db->get();
			if($query->num_rows()==1){
				return $query->row();
			}else{
			return false;
			}
			
			
			
	}
	
	
	
	
	
	
	
}